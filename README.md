# README #

This is a solver inspired by the puzzle in Capter 1 of Matt Parker's Things to Make and do in the Fourth Dimension.

### What is this repository for? ###

* This repository contains code to find polydivisible number(s) for a given base, if they exist.
* [Source code](https://ron_garrison@bitbucket.org/ron_garrison/polydivisible.git)


