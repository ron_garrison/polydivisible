"""Polydivisible solver."""

DIGITS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def unused_digits(value: str, base: int):
    """Return a list of unused digits in a number."""
    digits = list(DIGITS[1:base])
    for d in str(value):
        digits.remove(d)
    return digits


def increment_value(value: str, base: int):
    """Increment a number without reusing digits."""
    incremented = False
    digit_count = len(str(value))
    temp = list(str(value))
    while not incremented:
        available_digits = unused_digits("".join(temp), base)
        last_digit = temp[-1]

        # can we increment the least significatn digit?
        if available_digits and last_digit < available_digits[-1]:
            available_digits.append(last_digit)
            available_digits.sort()
            temp[-1] = available_digits[available_digits.index(last_digit) + 1]
            incremented = True
        else:
            # least significant digit can't be incremented, so drop it
            temp.pop(-1)
            if not temp:
                # we just rolled over, so add a digit
                raise OverflowError
                # digit_count += 1
                # incremented = True

    new_value = "".join(temp)  # turn the list of digits into a string
    while len(new_value) < digit_count:  # add back the popped digits.
        new_value = add_digit(new_value, base)
    return new_value


def add_digit(value: str, base: int):
    """Add a trailing digit without reusing digits."""
    digits = unused_digits(str(value), base)
    if digits:
        return str(value) + str(digits[0])
    else:
        raise OverflowError


def check_polydivisible(n: str, base: int):
    """Check if n is polydivisible in base."""
    for i in range(len(str(n))):
        # print(f"checking {n} with {i+1}")
        if int(n[: i + 1], base) % (i + 1):
            return False
    return True


for base in range(17):
    n = ""
    while len(n) < (base - 1):
        n = add_digit(n, base)
        polydivisible = False
        try:
            while not polydivisible:
                polydivisible = check_polydivisible(n, base)
                if polydivisible:
                    if len(n) == (base - 1):
                        print(f"base: {base}              n: {n}")
                    else:
                        print(f"base: {base}   intermediate: {n}")
                else:
                    n = increment_value(n, base)
        except Exception:
            print(f"base: {base} does not have any polydivisible numbers")
            break
